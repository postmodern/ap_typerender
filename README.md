# ActivityPub TypeRender
`ap_typerender` is a simple node-based tool designed with the express goal of rendering various activity objects from the ActivityPub protocol. The purpose is to serve as a handy reference for developers building protocol implementations to see how to render different activity types on their frontends.

This is a work-in-progress. Check back later for details!

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
